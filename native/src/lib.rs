extern crate zip;
use std::io::{Cursor, BufReader, Read};
use neon::prelude::*;

struct Arch {
    name: String,
    size: u64,
    buffer: Vec<u8>,
}

fn unzip(mut cx: FunctionContext) -> JsResult<JsArray> {
    let buffer: Handle<JsBuffer> = cx.argument(0)?;
    let arc_raw = cx.borrow(&buffer, |data| data.as_slice::<u8>());
    let reader = BufReader::new(Cursor::new(arc_raw));
    let mut result: Vec<Arch> = Vec::new();

    let mut archive = zip::ZipArchive::new(reader).unwrap();
    for i in 0..archive.len() {
        let mut file = archive.by_index(i).unwrap();
        let outpath = match file.enclosed_name() {
            Some(path) => path,
            None => continue
        };
        let mut arch = Arch {
                name: outpath.display().to_string(),
                size: file.size(),
                buffer: Vec::new(),
        };
        file.read_to_end(&mut arch.buffer).expect("write error.");
        result.push(arch);
    }

    let js_array = JsArray::new(&mut cx, result.len() as u32);

    for (i, o) in result.iter().enumerate() {
        let obj = JsObject::new(&mut cx);
        let name = cx.string(&o.name);
        let size = cx.number(o.size as f64);
        obj.set(&mut cx, "name", name).unwrap();
        obj.set(&mut cx, "size", size).unwrap();

        let mut buf = cx.buffer(o.buffer.len() as u32)?;
        cx.borrow_mut(&mut buf, |buf| {
            let buf = buf.as_mut_slice();
            buf.copy_from_slice(o.buffer.as_slice());
        });
        obj.set(&mut cx, "content", buf).unwrap();

        js_array.set(&mut cx, i as u32, obj).unwrap();
    }

    Ok(js_array)
}

register_module!(mut cx, {
    cx.export_function("unzip", unzip)
});
